# Better Tab v0.1.0a

A beautiful New-tab Chrome Extension developed

## Design Choices

### Design Changes

### Design Workflow

Chosen the following workflow to make our project eloquent.

- _Empathise_ - the needs - because it allows us to set aside one's opinions and gain real insight into users and their needs.
- _Define_ - the accumulated information - to analyse the observations and synthesise them to establish the core problem.
- _Ideate_ - generate ideas - to solve the problems with the rooted background of knowledge gathered in the past stages. By seeking an alternative way to view the problem and identify innovative solutions.
- _Prototype_ - create solutions - several inexpensive, scaled-down versions of the product until we finalise an exceptional solution.
- _Test_ - try the solution - the product rigorously using the best solutions identified in the Prototype phase. Whilst being flexible to return to previous stages in the process to make further iterations, alterations and refinements to rule out alternative solutions.
- _Build_ - finalise the solution - get feedbacks and present the build.

## Getting Started

Since it's a simple plugin, the mandetory prerequsites are quite less. However, having a good project structure is a vital whilst development any application. _Current version: v0.1.0a_

### Prerequisites

```
Node.js
React.js
Node Sass
```

### Installing

Clone the project

```
comming soon
```

Install the Dependencies

```
npm install
```

## Built With

- [Node JS](https://nodejs.org/en/) - JavaScript runtime
- [React JS](https://reactjs.org/) - A JavaScript library for building user interfaces

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository]().

## Authors

- **Siva Vadlamani** - [itsJade](https://bitbucket.org/account/user/DiggingNebula8/)

## License

This project is licensed under the Creative Commons Attribution Share Alike 4.0 **cc-by-sa-4.0** - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments